Vue.createApp({
    data(){
        return{
            inputText: '',
            anotherText: ''
        }
    },
    methods:{
        anotherInputValue(event){
            this.anotherText = event.target.value
        },
        inputValue(event){
            this.inputText = event.target.value
        },
        showAlert(){
            alert('Alert!')
        }
    }
}).mount('#assignment')