Vue.createApp({
    data(){
        return{
            goals:[],
            enteredValue:''
        };
    },
    methods:{
        addGoal(){
            console.log('ok');
            this.goals.push(this.enteredValue);
            this.enteredValue = '';
        }
    }
}).mount('#app');


// const buttonEl = document.querySelector('button');
// const listEl = document.querySelector('ul');
// const inputEl = document.querySelector('input');


// function addGoal(){
//     let inputVal = inputEl.value;
//     const newEl = document.createElement('li');
//     newEl.textContent = inputVal;
//     listEl.appendChild(newEl);
//     inputEl.value = '';
// }

// buttonEl.addEventListener('click', addGoal);