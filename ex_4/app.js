const app = Vue.createApp({
  data() {
    return {
      counter: 0,
      name: '',
      lastName: '',
      confirmInput: ''
    };
  },
  watch: {
    counter(value){
      if(value > 50){
        const that = this;
        setTimeout(function(){
          that.counter = 0;
        },2000)

      }
    }
  },
  computed:{
    fullName(){
      if(this.name === ''){
        return '';
      } 
        return this.name + ' ' + this.lastName;  
    }
  },
  methods: {
    outputFullName(){
      if(this.name === ''){
        return '';
      }
      return this.name + " Honcharuk";
    },
    reloadInput(){
      this.name = '';
      this.lastName = '';
    },
    getConfirmName(){
      this.confirmInput = this.name;
    },
    preventForm(){
      alert('Prevented!')
    },
    setName(event){
      // this.name = event.target.value + ' ' + lastName;
      this.name = event.target.value;
    },
    add(num){
       this.counter = this.counter + num;
    },
    reduce(num){
       this.counter = this.counter - num;
    }
  }
});

app.mount('#events');
