Vue.createApp({
    data(){
        return{
            name: 'Serhii',
            age: 26,
            imgLink: 'https://cdn.gamestatic.net/files/editor_uploads/%D0%A1%D0%BA%D1%80%D0%B8%D0%BD%D1%88%D0%BE%D1%82%D1%8B%20%D1%8F%D0%BD%D0%B2%D0%B0%D1%80%D1%8C%202019/alienblackout_1.jpg'
        }
    },
    methods:{
        getAge(){
            return this.age + 5;
        },
        getRandInt(){
            return Math.random();
        }
    }
}).mount('#assignment')