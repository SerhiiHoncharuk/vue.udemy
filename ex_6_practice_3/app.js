const app = Vue.createApp({
    data(){
        return{
            counter: 0,
            result: 'RESULT',
        }
    },
    methods:{
        add(num){
            this.counter = this.counter + num;
            console.log(this.counter);
        },
    },
    watch:{
        counter(){
            if(this.counter < 37){
                this.result = 'Not there yet!';
            } else if(this.counter == 37){
                this.result = 37;
            } else(
                this.result = 'Too much!'
            )
        },
        result(value){
            const that = this;
            setTimeout(function(){
                that.counter = 0;
            }, 5000);
        }
    },
});

app.mount('#assignment');

