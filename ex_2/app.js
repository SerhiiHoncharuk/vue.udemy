// Vue.createApp({
//     data(){
//         return{

//         }
//     }
// }).mount('#user-goal');

const app = Vue.createApp({
    data(){
        return{
            objectGoalA: 'I want to learn Vue!',
            objectGoalB: 'I want to sell my project',
            vueLink: 'https://www.google.com',
            linkL: 'Link'
        }
    },
    methods:{
        findGoal(){
            let random = Math.random();
            if(random < 0.5){
                // return 'Yep, ill do anything'
                return this.objectGoalA
            } else {
                // return 'Looser'
                return this.objectGoalB
            }
        }
    }
});

app.mount('#user-goal');